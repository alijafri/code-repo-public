# payl8r-technical-test-FE

The main approach that I've adopted to close to using more vanilla JS rather then use a framework. Using the List.js library to derive results. 
Although frameworks can also do the job I found it faster to execute it this way. Although vue.js does have good capability to do the same as shown here: https://vuejs.org/v2/guide/list.html
However, due to faster execution I opted to do it this way. 

Thanks,
Ali
