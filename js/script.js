// Create the List
const options = {
  valueNames: ["name", "industry", { name: 'logo', attr: 'src' }],
  item: '<a class="list__item" href="javascript:void(0)"><div class="name" style="font-weight:bold"></div><div class="industry"></div><img class="logo" style="width:20em"></a>',
};
// Populate the List
const values = [
    {
      name: 'Vodex',
      industry: 'retail',
      logo: 'images/vodex.png'
    },
    {
      name: 'Beauty Cutie',
      industry: 'beauty',
      logo: 'images/beautycutie.png'
    },
  
    {
      name: "Dermanc",
      industry: "beauty",
      logo: "images/dermace.png"
    },
    {
      name: "Evans Electrical",
      industry: "retail",
      logo: "images/evans.png"
    },
  
    {
      name: "Beauty Works",
      industry: "beauty",
      logo: "images/beautyworks.png"
    },
    {
      name: "AMC",
      industry: "retail",
      logo: "images/amc.png"
    },
 
    {
      name: "Nadine Merabi",
      industry: "fashion",
      logo: "images/nadinemerabi.png"
    },
    {
      name: "Skins & Needles",
      industry: "fashion",
      logo: "images/skinsnneedles.png"
    },
    {
      name: "Aimees",
      industry: "beauty",
      logo: "images/aimees.png"
    },
    {
      name: "Boda Skins",
      industry: "fashion",
      logo: "images/bodaskins.png"
    }
];

// Run the list with default sort
const retailerList = new List("retailerList", options, values);
retailerList.sort("name", {
  order: "asc"
});

// Create Filters
$('.filter').on('click',function(){
  let $q = $(this).attr('data-filter');
  if($(this).hasClass('active')){
    retailerList.filter();
    $('.filter').removeClass('active');
  } else {
    retailerList.filter(function(item) {
      return (item.values().industry == $q);
    });
    $('.filter').removeClass('active');
    $(this).addClass('active');
  }
});

// Return # of items
let $count = $(".count");
$count.append(retailerList.size());
retailerList.on("filterComplete", function() {
  $count.text(retailerList.update().matchingItems.length);
});